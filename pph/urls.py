from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from hub import views


urlpatterns = [
    path('', include('hub.urls')),
    path(settings.ADMIN_URL if not settings.DEBUG else 'admin/', admin.site.urls),
    path('accounts/', include('accounts.urls')),
    path('stats/', include('stats.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
