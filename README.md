### Steps to setup Postgres db:

#### Postgres
* `sudo -u postgres psql`
* `CREATE DATABASE porschepartshub;`

if there's no `<username>` in Posgres:
* `CREATE USER <username> WITH PASSWORD '<userpwd>';`
* `ALTER ROLE <username> SET client_encoding TO 'utf8';`
* `ALTER ROLE <username> SET default_transaction_isolation TO 'read committed';`
* `ALTER ROLE <username> SET timezone TO 'Europe/Moscow';`

then:
* `GRANT ALL PRIVILEGES ON DATABASE porschepartshub TO <username>;`
* `\q`

##### `local_settings.py`:
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'porschepartshub',
        'USER': '<username>',
        'PASSWORD': '<userpwd>',
        'HOST': '<postgres hostname or ip>',
        'PORT': '<postgres port (probably 5432)>',
    },
}


PAGINATE_BY = 12
ADMIN_URL = '<your_secret_admin_url>/'  # like 7ujhbvy654 or whatever you might memorize to access the admin site.
SECRET_KEY = '<your_super_secret_key>'
ALLOWED_HOSTS = ['list', 'of', 'your', 'allowed', 'hosts']
```

9. `./manage.py migrate`
10. `./manage.py createsuperuser`