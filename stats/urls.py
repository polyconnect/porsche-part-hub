from django.urls import path
from . import views

urlpatterns = [
    path('stock/', views.stock_stats, name='stock_stats'),
]