from django.contrib.auth.decorators import login_required
from django.db.models import Count, Sum, ExpressionWrapper, F, DecimalField
from django.shortcuts import render
from hub.models import VehicleModel


@login_required
def stock_stats(request):
    ppm = VehicleModel.objects.annotate(
        num_parts=Count('part'),
        qty_parts=Sum('part__quantity'),
        total_cost=ExpressionWrapper(Sum(F('part__quantity') * F('part__cost')), output_field=DecimalField()),
        total_price=ExpressionWrapper(Sum(F('part__quantity') * F('part__price')), output_field=DecimalField())
    ).order_by('vehicle_brand__title', 'vehicle_model')
    aggregates = ppm.aggregate(Sum('num_parts'), Sum('qty_parts'), Sum('total_cost'), Sum('total_price'))
    ctx = {'ppm': ppm, 'agg': aggregates}

    return render(request, 'stats/stock_stats.html', ctx)
