from django.shortcuts import redirect, render
from django.contrib.auth.models import User
from django.contrib import auth, messages

from urllib.parse import parse_qs, urlparse

# from django.contrib.auth.forms import UserCreationForm, AuthenticationForm


def signup(req):
    if req.method == 'POST':
        # user has info and wants an account now
        pw0 = req.POST.get('pw0')
        pw1 = req.POST.get('pw1')
        if pw0 and pw1 and pw0 == pw1:
            if username := req.POST.get('username'):
                if User.objects.filter(username=username):
                    messages.error(req, 'Failed to signup with the given username')
                    return render(req, 'accounts/signup.html')
                else:
                    user = User.objects.create_user(username=username, password=pw0)
                    auth.login(req, user)
                    return redirect('home')
        else:
            messages.error(req, 'Passwords must match and shouldn\'t be empty')
            return render(req, 'accounts/signup.html')

    else:
        # user wants to enter info
        return render(req, 'accounts/signup.html')


def login(req):
    if req.method == 'POST':
        urlp = urlparse(req.META.get('HTTP_REFERER'))
        urlq = parse_qs(urlp.query)
        nextpage = urlq.get('next', ['home'])[0]
        username = req.POST.get('username')
        password = req.POST.get('password')

        if not username:
            messages.error(req, 'Failed to parse username')
        if not password:
            messages.error(req, 'Failed to parse password')
        if not all((username, password)):
            return render(req, 'accounts/login.html')

        if user := auth.authenticate(username=username, password=password):
            auth.login(req, user)
            return redirect(nextpage)

        messages.error(req, 'Authentication failed')
        return render(req, 'accounts/login.html')

    else:
        return render(req, 'accounts/login.html')


def logout(req):
    # todo: need to route to homepage and don't forget to logout
    if req.method == 'POST':
        auth.logout(req)
        return redirect('home')
