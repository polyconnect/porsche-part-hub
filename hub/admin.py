from django.contrib import admin
from .models import VehicleBrand, VehicleModel, Part, SalesJournal


class PartAdmin(admin.ModelAdmin):

    list_display = ('part_name', 'part_number',
                    'quantity', 'cost', 'price', 'warehouse', 'rack')
    # raw_id_fields = ('main_uuid',)
    list_per_page = 50
    # list_filter = ('vehicle_model',)
    search_fields = ('vehicle_model__vehicle_model', 'part_name', 'part_number')


class SalesAdmin(admin.ModelAdmin):
    list_display = ('part', 'sale_date', 'quantity', 'cost', 'price')
    list_per_page = 50
    search_fields = ('part__part_name', 'part__vehicle_model__vehicle__model', 'part__part_number')


admin.site.register((VehicleBrand, VehicleModel))
admin.site.register(Part, PartAdmin)
admin.site.register(SalesJournal, SalesAdmin)


