import json

from django.db.models import Q
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import redirect, render, get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.views.decorators.csrf import ensure_csrf_cookie
from .models import VehicleBrand, VehicleModel, Part, PartForm, PartImage, SalesJournal

from urllib.parse import urlencode
from utils import q_for_fields

import platform
from django import __version__ as dj_version

decorators = [ensure_csrf_cookie, ]


def welcome(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        return render(request, 'accounts/login.html')


# expects part_list.html to exist
class PartListView(ListView):
    model = Part
    ordering = ['-reg_date']
    paginate_by = settings.PAGINATE_BY

    def get_context_data(self, **kwargs):
        ctx = super(PartListView, self).get_context_data(**kwargs)
        if q := self.request.GET.dict():
            q = dict(filter(lambda kv: kv[0] != 'page', q.items()))

            ctx.update({'q': urlencode(q)})

        total_qty = Part.objects.all().count()
        inactive_qty = Part.objects.filter(quantity__lt=1).count()

        ctx.update(
            {
                'total': total_qty,
                'inactive': inactive_qty,
                'py_version': platform.python_version(),
                'dj_version': dj_version,
            }
        )

        return ctx

    def get_queryset(self):
        if 'action_search' in self.request.GET:
            search_list = self.request.GET.getlist('search_field')
            in_stock = self.request.GET.getlist('in_stock', ['yes'])[0]

            if in_stock == 'yes':
                in_stock_q = Q(quantity__gt=0)
            elif in_stock == 'no':
                in_stock_q = Q(quantity__lte=0)
            else:
                in_stock_q = Q()

            q_string = ' '.join(search_list)

            q = q_for_fields(q_string, ('part_number', 'part_name', 'vehicle_model__vehicle_brand__title',
                                        'vehicle_model__vehicle_model', 'remarks'))
            q &= in_stock_q

            qs = Part.objects.filter(q)
        elif 'action_clear' in self.request.GET:
            qs = Part.objects.all()
        elif 'inactive_search' in self.request.GET:
            qs = Part.objects.filter(quantity__lt=1)
        elif qd := self.request.GET.dict():
            qd = dict(filter(lambda kv: kv[0] != 'page', qd.items()))
            q = Q(**qd)
            qs = Part.objects.filter(q)
        else:
            qs = Part.objects.all()

        return qs.order_by('part_number')


# expects salesjournal_list.html to exist
class SalesListView(LoginRequiredMixin, ListView):
    model = SalesJournal


# View with part photo and table of part details
class PartDetailView(DetailView):
    model = Part


class PartDeleteView(LoginRequiredMixin, DeleteView):
    model = Part
    success_url = '/'


# Part create/update form : PartForm
# expects part_detail.html to exist
@method_decorator(decorators, name='dispatch')
class PartCreateView(LoginRequiredMixin, CreateView):
    model = Part
    form_class = PartForm

    def get_context_data(self, **kwargs):
        ctx = super(PartCreateView, self).get_context_data(**kwargs)
        brands = VehicleBrand.objects.order_by('title').values('id', 'title')
        ctx.update({'brands': brands, 'mode': 'create'})
        return ctx

    def form_valid(self, form):
        form.instance.user_added = self.request.user
        return super().form_valid(form)

    success_url = '/'


# Part create/update form : PartForm
# expects part_detail.html to exist
@method_decorator(decorators, name='dispatch')
class PartUpdateView(LoginRequiredMixin, UpdateView):
    model = Part
    form_class = PartForm

    def get_context_data(self, **kwargs):
        ctx = super(PartUpdateView, self).get_context_data(**kwargs)
        brands = VehicleBrand.objects.all().values('id', 'title')
        model = self.object.vehicle_model
        brand = model.vehicle_brand
        models = VehicleModel.objects.filter(vehicle_brand=brand)
        ctx.update({'brands': brands, 'mode': 'update', 'models': models, 'model_id': model.id, 'brand_id': brand.id})
        return ctx

    def form_valid(self, form):
        form.instance.user_added = self.request.user
        if not form.instance.photo:
            try:
                p = Part.objects.get(pk=form.instance.id)
                if photo := p.photo:
                    form.instance.photo = photo
            except Part.DoesNotExist:
                pass

        return super().form_valid(form)

    success_url = '/'


def models_by_brand(_, brand_id):
    v_models = list({'id': i['id'], 'text': i['vehicle_model']} for i in
                    VehicleModel.objects.filter(vehicle_brand__id=brand_id).values('id', 'vehicle_model'))
    return HttpResponse(json.dumps(v_models))


@login_required
def sell(request, pk):
    part = get_object_or_404(Part, pk=pk)
    if request.method == 'POST':
        if part.quantity > 0:
            part.quantity -= 1
            part.save()

            SalesJournal.objects.create(part=part, quantity=1, cost=part.cost, price=part.price)

        return redirect('part_detail', pk=pk)

    ctx = {'part': part}
    return render(request, 'hub/part_detail.html', ctx)


def add_brand(_, new_brand):
    VehicleBrand.objects.create(title=new_brand)
    brands = ({'id': pk, 'text': title} for pk, title
              in VehicleBrand.objects.order_by('title').values_list('id', 'title'))
    return HttpResponse(json.dumps(list(brands)))


def add_model(_, brand_id, new_model):
    brand = VehicleBrand.objects.get(pk=brand_id)
    VehicleModel.objects.create(vehicle_model=new_model, vehicle_brand=brand)
    models = ({'id': pk, 'text': title} for pk, title in VehicleModel.objects.filter(vehicle_brand=brand)
                                                                             .order_by('vehicle_model')
                                                                             .values_list('id', 'vehicle_model'))
    return HttpResponse(json.dumps(list(models)))


def add_photo(request):
    image = request.FILES.get('photo')
    pi = PartImage.objects.create(photo=image)
    return HttpResponse(pi.id)


def add_photo_to_part(request, part_id):
    return add_photo(request)
