const brand_dd = document.getElementById('id_brand');
const model_dd = document.getElementById('id_vehicle_model');

const add_brand = document.getElementById('add_brand_btn');
const add_model = document.getElementById('add_model_btn');

const new_brand_textbox = document.getElementById('new_brand');
const new_model_textbox = document.getElementById('new_model');

const img_input = document.getElementById('id_image');
const submit_btn = document.getElementById('submit-id-submit');
const submit_photo_btn = document.getElementById('submit-photo');

var tmpf = undefined; // resized file

const compress = e => {
    const dHeight = 800;

    const file = e.target.files[0];
    const fileName = file.name;
    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = evt => {
        const img = new Image();
        img.src = evt.target.result;
        img.onload = () => {
            const imgW = img.width;
            const imgH = img.height;

            const elem = document.createElement('canvas');

            elem.height = dHeight;
            elem.width = Math.round(dHeight * imgW / imgH);
            const dWidth = elem.width;
            const ctx = elem.getContext('2d');
            const sx = 0;
            const sy = 0;
            const dx = 0;
            const dy = 0;

            ctx.drawImage(img, sx, sy, imgW, imgH, dx, dy, dWidth, dHeight);
            ctx.canvas.toBlob(blob => {
                const file = new File([blob], fileName, {
                    type: 'image/jpeg',
                    lastModified: Date.now()
                });
                tmpf = file;

            }, 'image/jpeg');
        },
        reader.onerror = error => console.log(error);
    };
}

//https://docs.djangoproject.com/en/3.0/ref/csrf/#acquiring-the-token-if-csrf-use-sessions-and-csrf-cookie-httponly-are-false
const getCookie = name => {
  let cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    const cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i].trim();
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}


const updateSelect = (select_obj, data, add_stub) => {
    // remove previous
    while (select_obj.firstChild) {
        select_obj.removeChild(select_obj.lastChild);
    }

    // add stub
    if (add_stub) {
        var opt = document.createElement('option');
        opt.value = "";
        opt.innerHTML = "---------";
        select_obj.appendChild(opt)
    }

    // add choices
    data.forEach(i => {
        opt = document.createElement('option');
        opt.value = i.id;
        opt.innerHTML = i.text;
        select_obj.appendChild(opt);
    });
}

const onBrandSelect = e => {
    e.preventDefault();
    const brandId = e.target.value;
    setModelByBrandId(brandId);
}

const onAddBrandClick = e => {
    e.preventDefault();
    const new_brand = new_brand_textbox.value;
    new_brand_textbox.value = '';
    if (new_brand) {
        const xhr = new XMLHttpRequest();
        const url = 'new_brand/' + new_brand;
        xhr.open('get', url, true);

        xhr.onload = () => {
            data = JSON.parse(xhr.responseText);
            updateSelect(brand_dd, data, false);
            for (i = 0; i < data.length; ++i) {
                if (data[i].text == new_brand) {
                    brand_dd.selectedIndex = i;
                    setModelByBrandId(data[i].id);
                    break;
                }
            }
        }
        xhr.send();
    }
}

const onAddModelClick = e => {
    e.preventDefault();
    const brand_id = parseInt(brand_dd.value);
    if (brand_id) {
        const new_model = new_model_textbox.value;
        if (new_model) {
            new_model_textbox.value = '';
            const xhr = new XMLHttpRequest();
            const url = 'new_model/' + brand_id + '/' + new_model;
            xhr.open('get', url, true);

            xhr.onload = () => {
                data = JSON.parse(xhr.responseText);
                updateSelect(model_dd, data, true);
                for (i = 0; i < data.length; ++i) {
                    if (data[i].text == new_model) {
                        model_dd.selectedIndex = i+1;  // Since ------ is the 0th index
                        break;
                    }
                }
            }
            xhr.send();
        }
    }
}

const setModelByBrandId = brandId => {
    const xhr = new XMLHttpRequest();
    const url = brandId + '/get_models_for';
    xhr.open('get', url, true);

    xhr.onload = () => {
        if (xhr.status == 200) {
            data = JSON.parse(xhr.responseText);
            updateSelect(model_dd, data, true);
            model_dd.selectedIndex = 0;
        }
    }
    xhr.send();
}

brand_dd.addEventListener('input', onBrandSelect);
add_brand.addEventListener('click', onAddBrandClick);
add_model_btn.addEventListener('click', onAddModelClick);
img_input.addEventListener('change', compress, false);


if (mode == 'create') {
    for (i = 0; i < brand_dd.length; ++i) {
        if (brand_dd[i].label == 'Porsche') {
            brand_dd.selectedIndex = i;
            break;
        }
    }
    setModelByBrandId(1);

} else if (mode == 'update' && model_id && brand_id) {
    model_dd.value = model_id;
    brand_dd.value = brand_id;
}

submit_photo_btn.addEventListener('click', e => {
    e.preventDefault();
    const partForm = document.getElementById('id_part_form');

    if (!tmpf) {
        // either create or update, submit partForm
        partForm.submit()
    } else {
        const photoform = document.getElementById('id_part_form');
        var photoinput = [...photoform.childNodes].filter(chn => (chn.type == 'hidden' && chn.name == 'photo'));

        if (photoinput) {

//            const csrf = partForm.firstElementChild;
            const csrftoken = getCookie('csrftoken');

            const vModel = document.getElementById('id_vehicle_model')
            const partName = document.getElementById('id_part_name')
            const partNumber = document.getElementById('id_part_number')
            const cost = document.getElementById('id_cost')
            const price = document.getElementById('id_price')
            const rack = document.getElementById('id_rack')

            const errors = [];

            if (!vModel.value) errors.push('* Не выбрана модель');
            if (!partName.value) errors.push('* Не указано название');
            if (!partNumber.value) errors.push('* Не указан номер запчасти');
            if (!cost.value) errors.push('* Не указана закупка');
            if (!price.value) errors.push('* Не указана продажа');
            if (!rack.value) errors.push('* Не указан стеллаж');

            if (errors.length > 0) {
                var msg = 'Не хватает данных:\n';
                msg += errors.join('\n');
                alert(msg);
                return
            }

            photoinput = photoinput[0];
            var fd = new FormData();
            fd.append('photo', tmpf);
//            fd.append(csrf.name, csrf.value); // csrf token

            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'add_photo');
            xhr.setRequestHeader('X-CSRFToken', csrftoken);

            xhr.onload = () => {
                if (xhr.status == 200) {
                    photo_id = parseInt(xhr.responseText);
                    photoinput.value = photo_id;
                    partForm.submit();
                }
            };
            xhr.send(fd);
        } else {
            partForm.submit();
        }
    }
});
