from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, HTML, Hidden

from django.contrib.auth.models import User
from django.db import models
from django.forms import ModelForm
from django.urls import reverse
from django.utils import timezone


class VehicleBrand(models.Model):
    title = models.CharField('Марка', max_length=100, unique=True, db_index=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Марка"
        verbose_name_plural = "Марки"


class VehicleModel(models.Model):
    vehicle_model = models.CharField('Модель', max_length=100, db_index=True)
    vehicle_brand = models.ForeignKey(to=VehicleBrand, on_delete=models.PROTECT, verbose_name='Марка', db_index=True)

    def __str__(self):
        return self.vehicle_model

    class Meta:
        verbose_name = "Модель"
        verbose_name_plural = "Модели"


class PartImage(models.Model):
    photo = models.ImageField('Фото', upload_to='images/')


class Part(models.Model):
    part_name = models.CharField('Название', max_length=255, db_index=True)
    part_number = models.CharField('Номер запчасти', max_length=255, db_index=True)
    vehicle_model = models.ForeignKey(to=VehicleModel, on_delete=models.PROTECT, verbose_name='Модель', db_index=True)
    quantity = models.IntegerField('Количество', default=1)
    cost = models.DecimalField('Закупка', max_digits=12, decimal_places=2)
    price = models.DecimalField('Продажа', max_digits=12, decimal_places=2)

    MG, RI = 'МГ', 'РИ'
    WAREHOUSES = [(MG, 'Склад МГ'), (RI, 'Склад РИ')]

    warehouse = models.CharField('Склад', max_length=2, choices=WAREHOUSES, default=RI, db_index=True)
    rack = models.CharField('Стеллаж', max_length=5, db_index=True)
    photo = models.ForeignKey(to=PartImage, on_delete=models.PROTECT, verbose_name='Фото з/ч', null=True, blank=True)

    reg_date = models.DateTimeField(default=timezone.now)
    remarks = models.TextField('Примечания', null=True, blank=True)

    user_added = models.ForeignKey(User, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.part_name}, {self.vehicle_model.vehicle_brand.title} {self.vehicle_model.vehicle_model}'

    class Meta:
        verbose_name = "Зап. часть"
        verbose_name_plural = "Зап. части"

    def get_absolute_url(self):
        return reverse('part_detail', kwargs={'pk': self.pk})


class PartForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PartForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_class = 'my-4'
        self.helper.form_id = 'id_part_form'

        self.helper.layout = Layout(
            HTML("""
                <div id="div_id_vehicle_model" class="form-group">
                    <label for="id_vehicle_model" class=" requiredField">Модель<span class="asteriskField">*</span></label>
                    <div class="row mx-auto">
                        <select name="vehicle_model" class="select form-control col-5" required="" id="id_vehicle_model">
                            <option value="">---------</option>
                            {% for item in models %}
                                <option value="{{ item.id }}">{{ item.vehicle_model }}</option>
                            {% endfor %}
                        </select>
                        <input type="text" class="textinput ml-2 form-control col-5" id="new_model">
                        <button id="add_model_btn" class="btn btn-outline-info btn-sm col-1 ml-2">+</button>
                    </div>
                </div>
                """),
            'part_name', 'part_number', 'quantity', 'cost', 'price', 'warehouse', 'rack',
            Hidden('photo', value=""),
            Field('remarks', rows="2"),
        )

    class Meta:
        model = Part
        exclude = ('reg_date', 'user_added',)


class SalesJournal(models.Model):
    part = models.ForeignKey(Part, on_delete=models.PROTECT, verbose_name='Запчасть', db_index=True)
    sale_date = models.DateTimeField('Дата', default=timezone.now)
    quantity = models.IntegerField('Количество', default=1)
    cost = models.DecimalField('Закупка', max_digits=12, decimal_places=2)
    price = models.DecimalField('Продажа', max_digits=12, decimal_places=2)

    def __str__(self):
        return self.part.part_name

    class Meta:
        verbose_name = "Продажа"
        verbose_name_plural = "Продажи"
