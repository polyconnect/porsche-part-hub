# Generated by Django 3.0.5 on 2020-04-07 06:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hub', '0007_auto_20200406_1447'),
    ]

    operations = [
        migrations.CreateModel(
            name='PartImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.ImageField(upload_to='images/', verbose_name='Фото')),
            ],
        ),
        migrations.AlterField(
            model_name='part',
            name='image',
            field=models.ImageField(null=True, upload_to='images/', verbose_name='Фото'),
        ),
        migrations.AlterField(
            model_name='part',
            name='remarks',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='part',
            name='photo',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='hub.PartImage', verbose_name='Фото з/ч'),
        ),
    ]
