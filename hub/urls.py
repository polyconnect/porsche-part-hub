from django.urls import path
from . import views
from .views import PartListView, PartDetailView, PartCreateView, PartUpdateView, PartDeleteView, SalesListView

urlpatterns = [
    path('', PartListView.as_view(), name='home'),
    path('<int:pk>', PartDetailView.as_view(), name='part_detail'),
    path('create', PartCreateView.as_view(), name='part_create'),
    path('<int:pk>/update', PartUpdateView.as_view(), name='part_update'),
    path('<int:pk>/delete', PartDeleteView.as_view(), name='part_delete'),
    path('<int:pk>/sell', views.sell, name='sell'),

    path('<int:brand_id>/get_models_for', views.models_by_brand),
    path('new_brand/<str:new_brand>', views.add_brand),
    path('new_model/<int:brand_id>/<str:new_model>', views.add_model),
    path('add_photo', views.add_photo),
    path('<int:part_id>/add_photo', views.add_photo_to_part),
    path('sales/', SalesListView.as_view(), name='sales'),
]
