import re
from django.db.models import Q
from functools import reduce


def get_Q(field, condition, word):
    return Q(**{f'{field}__{condition}': word})


def q_for_field(q_string, field, case_sensitive=False):
    splitter = re.compile(r'[^a-zA-Zа-яА-Я0-9-_]+')
    cond = 'contains' if case_sensitive else 'icontains'
    return reduce(lambda x, y: x | y, (get_Q(field, cond, word) for word in splitter.split(q_string)))


def q_for_fields(q_string, target_fields, case_sensitive=False):
    splitter = re.compile(r'[^a-zа-яА-ЯA-Z0-9-_]+')
    condition = 'contains' if case_sensitive else 'icontains'

    if not all((q_string, target_fields)):
        return Q()

    return reduce(lambda x, y: x | y, (get_Q(t_field, condition, word)
                                       for word in splitter.split(q_string)
                                       for t_field in target_fields))

